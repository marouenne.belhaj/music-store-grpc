package db

import (
	"context"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Connection interface {
	GetCollection(collectionName string) *mongo.Collection
}

type connection struct {
	config     Config
	client     *mongo.Client
	database   *mongo.Database
	collection *mongo.Collection
}

func NewConnection(collectionName string) (Connection, error) {
	config := NewConfig()
	client, err := mongo.NewClient(options.Client().ApplyURI(config.ConnectionString()))
	if err != nil {
		return nil, err
	}

	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancel()
	err = client.Connect(ctx)
	if err != nil {
		return nil, err
	}

	err = client.Ping(ctx, nil)
	if err != nil {
		return nil, err
	}

	fmt.Println("Connected to MongoDB")
	return &connection{config: config, client: client, database: client.Database(config.DbName())}, nil
}

func (c *connection) GetCollection(collectionName string) *mongo.Collection {
	return c.database.Collection(collectionName)
}