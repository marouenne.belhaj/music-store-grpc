// Package main implements a server for Artist service.
package main1

import (
	"context"
	"fmt"
	"log"
	"net"

	"google.golang.org/grpc"

	pb "gitlab.com/music-store-grpc/artistGRPC"
	controllers "gitlab.com/music-store-grpc/controllers"
	converter "gitlab.com/music-store-grpc/converter"
	db "gitlab.com/music-store-grpc/db"
)

// artistServer is used to implement pb.ArtistServiceServer
type artistServer struct {
	pb.UnimplementedArtistServiceServer
	artistController controllers.ArtistController
}

func NewArtistServer(artistController controllers.ArtistController) *artistServer {
	var artistServer artistServer
	artistServer.artistController = artistController
	return &artistServer
}

func (artistServer *artistServer) GetArtists(ctx context.Context, in *pb.GetArtistsRequest) (*pb.GetArtistsResponse, error) {

	artists, err := artistServer.artistController.GetArtists()
	if err != nil {
		return nil, err
	}

	var protoArtists []*pb.Artist
	for _, artist := range artists {
		protoArtist := converter.BsonArtistToPbArtist(&artist)
		protoArtists = append(protoArtists, &protoArtist)
	}

	res := &pb.GetArtistsResponse{
		Artists: protoArtists,
	}
	return res, nil

}

func (artistServer *artistServer) GetArtistById(ctx context.Context, in *pb.GetArtistByIdRequest) (*pb.GetArtistByIdResponse, error) {
	id := in.GetId()

	artist, err := artistServer.artistController.GetArtistById(id)
	if err != nil {
		return nil, err
	}

	protoArtist := converter.BsonArtistToPbArtist(&artist)

	response := &pb.GetArtistByIdResponse{
		Artist: &protoArtist,
	}

	return response, nil
}

func (artistServer *artistServer) CreateArtist(ctx context.Context, in *pb.CreateArtistRequest) (*pb.CreateArtistResponse, error) {

	protoArtist := in.GetArtist()
	bsonArtist, err := converter.PbArtistToBsonArtist(protoArtist)
	if err != nil {
		return nil, err
	}

	insertedID, err := artistServer.artistController.CreateArtist(bsonArtist)
	if err != nil {
		return nil, err
	}

	response := &pb.CreateArtistResponse{
		Message: "Artist created successfully.",
		Id:      insertedID.Hex(),
	}

	return response, nil
}

func main() {
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", 8080))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	// create the gRPC server
	grpcServer := grpc.NewServer()
	// Register the artist server with the gRPC server

	connection, _ := db.NewConnection("Artists")

	artistController := controllers.NewArtistController(connection)
	artistServer := NewArtistServer(artistController)

	pb.RegisterArtistServiceServer(grpcServer, artistServer)

	log.Printf("server listening at %v", lis.Addr())
	// start accepting Registred requests types
	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
