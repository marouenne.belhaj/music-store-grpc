package converter

import (
	"fmt"

	pb "gitlab.com/music-store-grpc/artistGRPC"
	models "gitlab.com/music-store-grpc/models"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func PbArtistToBsonArtist(pbArtist *pb.Artist) (models.Artist, error) {
    id, err := primitive.ObjectIDFromHex(pbArtist.GetId())
    if err != nil {
        return models.Artist{}, fmt.Errorf("error converting id from string to primitive.ObjectID: %v", err)
    }

    bsonArtist := models.Artist{
        ID:         id,
        ArtistName: pbArtist.GetArtistName(),
        FirstName:  pbArtist.GetFirstName(),
        LastName:   pbArtist.GetLastName(),
        Bio:        pbArtist.GetBio(),
    }

    return bsonArtist, nil
}

func BsonArtistToPbArtist(bsonArtist *models.Artist) pb.Artist {
	return pb.Artist{
				Id:        	bsonArtist.ID.Hex(),
				ArtistName: bsonArtist.ArtistName,
				FirstName:  bsonArtist.FirstName,
				LastName:   bsonArtist.LastName,
				Bio:        bsonArtist.Bio,
			}
}

func ConvertCreateArtistResponseJSON(pb.CreateArtistResponse) {
    
}