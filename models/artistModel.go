package models

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Defining Artist struct
type Artist struct {
	ID         primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	ArtistName string             `json:"artistName" bson:"artistName"`
	FirstName  string             `json:"firstName" bson:"firstName"`
	LastName   string             `json:"lastName" bson:"lastName"`
	Bio        string             `json:"bio" bson:"bio"`
}