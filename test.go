package main

import (
	"fmt"
	"image"
	"image/color"
	"image/draw"
	"image/png"
	"os"

	qrcode "github.com/skip2/go-qrcode"
)

func main() {
	// Generate a QR code
	qr, err := qrcode.New("https://example.com", qrcode.Highest)
	if err != nil {
		fmt.Println("Failed to generate QR code:", err)
		return
	}

	// Load your image
	imageFile, err := os.Open("path/to/image.png")
	if err != nil {
		fmt.Println("Failed to load image:", err)
		return
	}
	defer imageFile.Close()

	img, _, err := image.Decode(imageFile)
	if err != nil {
		fmt.Println("Failed to decode image:", err)
		return
	}

	// Convert the QR code to an image
	qrImage := qr.ToImage(256) // You can adjust the size (256 here) as needed

	// Create a colored version of the QR code
	coloredQR := image.NewRGBA(qrImage.Bounds())
	draw.Draw(coloredQR, coloredQR.Bounds(), qrImage, image.Point{}, draw.Src)

	// Modify the coloredQR image to apply colored squares
	applyColoredSquares(coloredQR)

	// Add the image to the center of the QR code
	overlayImageAtCenter(coloredQR, img)

	// Save the resulting image to a file
	outputFile, err := os.Create("output.png")
	if err != nil {
		fmt.Println("Failed to create output file:", err)
		return
	}
	defer outputFile.Close()

	err = png.Encode(outputFile, coloredQR)
	if err != nil {
		fmt.Println("Failed to encode output image:", err)
		return
	}
}

func applyColoredSquares(qr *image.RGBA) {
	// Change the color of squares as needed
	squareColor := color.RGBA{R: 0, G: 255, B: 0, A: 255}

	for y := 0; y < qr.Bounds().Dy(); y++ {
		for x := 0; x < qr.Bounds().Dx(); x++ {
			c := qr.At(x, y)
			_, _, _, a := c.RGBA()
			if a > 0 {
				qr.Set(x, y, squareColor)
			}
		}
	}
}

func overlayImageAtCenter(qr *image.RGBA, img image.Image) {
	imgBounds := img.Bounds()
	qrBounds := qr.Bounds()

	offsetX := (qrBounds.Dx() - imgBounds.Dx()) / 2
	offsetY := (qrBounds.Dy() - imgBounds.Dy()) / 2

	draw.Draw(qr, imgBounds.Add(image.Point{X: offsetX, Y: offsetY}), img, image.Point{}, draw.Over)
}
