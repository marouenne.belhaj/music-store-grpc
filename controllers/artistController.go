package controllers

import (
	"context"
	"fmt"

	db "gitlab.com/music-store-grpc/db"
	models "gitlab.com/music-store-grpc/models"

	"time"

	//"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type ArtistController interface {
	GetArtists() ([]models.Artist, error)
	GetArtistById(id string) (models.Artist, error)
	CreateArtist(artist models.Artist) (primitive.ObjectID, error)
}

type artistController struct {
	artistConnection db.Connection
}

func NewArtistController(artistConnection db.Connection) ArtistController {
	return &artistController{artistConnection: artistConnection}
}

func (a *artistController) GetArtists() ([]models.Artist, error) {
	var collection = a.artistConnection.GetCollection("Artists")

	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancel()

	cursor, err := collection.Find(ctx, bson.D{})
	if err != nil {
		return nil, err
	}

	var artists []models.Artist
	for cursor.Next(context.Background()) {
		var artist models.Artist
		if err := cursor.Decode(&artist); err != nil {
			return nil, err
		}
		artists = append(artists, artist)
	}

	return artists, nil
}

func (a *artistController) GetArtistById(id string) (models.Artist, error) {
	var artist models.Artist
	var collection = a.artistConnection.GetCollection("Artists")

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	primId, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return artist, fmt.Errorf("problem converting Id: %v", err)
	}

	filter := bson.M{"_id": primId}
	err = collection.FindOne(ctx, filter).Decode(&artist)
	if err != nil {
		return artist, fmt.Errorf("problem finding artist: %v", err)
	}

	return artist, nil
}

func (a *artistController) CreateArtist(artist models.Artist) (primitive.ObjectID, error)  {
	var collection = a.artistConnection.GetCollection("Artists")

	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancel()

	result, err := collection.InsertOne(ctx, artist)
	if err != nil {
		return primitive.NilObjectID, fmt.Errorf("error while inserting Artist")
	}

	oid, ok := result.InsertedID.(primitive.ObjectID)
	if !ok {
		return primitive.NilObjectID, fmt.Errorf("error while getting inserted Artist ID")
	}

	return oid, nil
}

/*
func (a *ArtistRequest) UpdateOneById() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		_, cancel := context.WithTimeout(context.Background(), 1*time.Second)
		defer cancel()

		// Get the ID parameter from the request URL
		assetId := mux.Vars(r)["id"]
		id, err := primitive.ObjectIDFromHex(assetId)
		if err != nil {
			http.Error(w, "Invalid artist ID parameter", http.StatusBadRequest)
			return
		}

		var updatedArtist models.Artist
		errDecode := json.NewDecoder(r.Body).Decode(&updatedArtist)
		if errDecode != nil {
			http.Error(w, "Error decoding artists ", http.StatusBadRequest)
			return
		}

		modifiedCount, err := a.artistDAO.UpdateOneById(id, updatedArtist)

		if modifiedCount == 0 {
			http.Error(w, "Artist not found", http.StatusNotFound)
			return
		}

		// Write a response
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		response := map[string]interface{}{
			"message": "Artist updated successfully",
			"id":      id,
		}
		json.NewEncoder(w).Encode(response)
	}
}

func (a *ArtistRequest) DeleteOneById() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		_, cancel := context.WithTimeout(context.Background(), 1*time.Second)
		defer cancel()

		// Get the id parameter from the request URL
		assetd := mux.Vars(r)["id"]
		id, err := primitive.ObjectIDFromHex(assetd)
		if err != nil {
			http.Error(w, "Error decoding artists ", http.StatusInternalServerError)
			return
		}

		deletedCount, err := a.artistDAO.DeleteOneById(id)

		if deletedCount == 0 {
			http.Error(w, "Artist not found", http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		response := map[string]interface{}{
			"message": "Artist deleted successfully",
			"id":      id,
		}
		json.NewEncoder(w).Encode(response)
	}
}
*/
