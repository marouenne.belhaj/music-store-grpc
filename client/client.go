package main

import (
	"context"
	"fmt"
	"log"
	"time"

	pb "gitlab.com/music-store-grpc/artistGRPC"
	converter "gitlab.com/music-store-grpc/converter"
	"gitlab.com/music-store-grpc/models"
	"google.golang.org/grpc"
)

func main() {
	address := "localhost:8080"

	conn, err := grpc.Dial(address, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	client := pb.NewArtistServiceClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	// Calling the GetArtists method
	getArtistsResponse, err := client.GetArtists(ctx, &pb.GetArtistsRequest{})
	if err != nil {
		log.Fatalf("could not get artists: %v", err)
	}
	fmt.Printf("Artists: %+v\n", getArtistsResponse.Artists)

	// Calling the GetArtistById method
	getArtistByIdResponse, err := client.GetArtistById(ctx, &pb.GetArtistByIdRequest{Id: "64455e3e45395e730f6ad995"})
	if err != nil {
		log.Fatalf("could not get artist by Id: %v", err)
	}
	fmt.Println(converter.PbArtistToBsonArtist(getArtistByIdResponse.Artist))

	// Calling the CreateArtist method
	protoArtist := converter.BsonArtistToPbArtist(&models.Artist{
																	ArtistName: "TEST INSERT",
																	FirstName : "TEST INSERT",
																	LastName: "TEST INSERT",
																	Bio : "TEST INSERT",
																})
	createArtistResponse, err := client.CreateArtist(ctx, &pb.CreateArtistRequest{Artist: &protoArtist})
	if err != nil {
		log.Fatalf("could not get artist by Id: %v", err)
	}
	fmt.Println(createArtistResponse)
}