package main

import (
	"fmt"
	"image"
	"image/color"
	"image/draw"
	"image/gif"
	"image/jpeg"
	"image/png"
	"os"
	"path/filepath"
	"strings"

	"github.com/nfnt/resize"
	"github.com/skip2/go-qrcode"
	"github.com/fogleman/gg"
)

type CodeController struct{}

func NewCodeController() CodeController {
	return CodeController{}
}

// Generate a QR code with an icon in the middle
func (g *CodeController) GetIcon(content string, qrCodeId string, logoPath string, savePath string) {

	var (
		bgImg		image.Image
		offset		image.Point
		iconFile 	*os.File
		iconImg		image.Image
	)

	// Content to be encoded
	qrCode, err := qrcode.New(content, qrcode.Highest) // Recovery Level is the Highest
	if err != nil {
		//return nil, errors.New("Failed to create QR code")
		return
	}

	// Disable borders
	qrCode.DisableBorder = false
	//qrCode.DisableBorder = true 
	bgImg = qrCode.Image(256)

	// Modify the size of the image
	iconFile, err = os.Open(logoPath)
	if err != nil {
		fmt.Println("1", err)
	}
	defer iconFile.Close()

	iconImg, err = jpeg.Decode(iconFile)
	if err != nil {
		fmt.Println("2", err)
	}
	// Modify the size of the image
	iconImg = resize.Resize(60, 60, iconImg, resize.Lanczos3)

	//得到背景图的大小 Get the size of the background image
	b := bgImg.Bounds()
	fmt.Println("ratatatattatataaa", b)
	//居中设置icon到二维码图片 Center set icon to QR code image
	offset = image.Pt((b.Max.X-iconImg.Bounds().Max.X)/2, (b.Max.Y-iconImg.Bounds().Max.Y)/2)
	fmt.Println("ratatatattatataaa", b.Max.X)
	fmt.Println("ratatatattatataaa", iconImg.Bounds().Max.X)
	offset = image.Pt((b.Max.X-iconImg.Bounds().Max.X)/2, (b.Max.Y-iconImg.Bounds().Max.Y)/2)
	fmt.Println("ratatatattatataaa", offset)

	m := image.NewRGBA(b)
	applyColoredSquares(m)
	applyGradient(m)
	draw.Draw(m, b, bgImg, image.Point{X: 0, Y: 0}, draw.Src)
	applyGradient(m)
	draw.Draw(m, iconImg.Bounds().Add(offset), iconImg, image.Point{X: 0, Y: 0}, draw.Over)
	//applyColoredSquares(m)

	//save image
	result := fmt.Sprintf("%s%s%s", savePath, qrCodeId, ".png")
	SaveImage(result, m)

	//errsave := SaveImage(result, m)
	/*
		if errsave != nil {
			fmt.Println(errsave)
		}
		//显示图片
		pngurl := "/static/images/q2.png"
		html := "<img src='" + pngurl + "' />"
		c.Header("Content-Type", "text/html; charset=utf-8")
		c.String(200, html)
	*/
	return
}

// 保存image
func SaveImage(p string, src image.Image) error {
	f, err := os.OpenFile(p, os.O_SYNC|os.O_RDWR|os.O_CREATE, 0666)
	if err != nil {
		return err
	}
	defer f.Close()
	ext := filepath.Ext(p)
	if strings.EqualFold(ext, ".jpg") || strings.EqualFold(ext, ".jpeg") {
		err = jpeg.Encode(f, src, &jpeg.Options{Quality: 80})
	} else if strings.EqualFold(ext, ".png") {
		err = png.Encode(f, src)
	} else if strings.EqualFold(ext, ".gif") {
		err = gif.Encode(f, src, &gif.Options{NumColors: 256})
	}
	return err
}

func applyColoredSquares(qr *image.RGBA) {
	// Change the color of squares as needed
	squareColor := color.RGBA{R: 0, G: 255, B: 0, A: 255}

	for y := 0; y < qr.Bounds().Dy(); y++ {
		for x := 0; x < qr.Bounds().Dx(); x++ {
			c := qr.At(x, y)
			_, _, _, a := c.RGBA()
			if a > 0 {
				qr.Set(x, y, squareColor)
			}
		}
	}
}

func applyGradient(qr *image.RGBA) {
	gradient := gg.NewContext(qr.Bounds().Dx(), qr.Bounds().Dy())

	// Define the gradient colors and direction
	startColor := gg.NewLinearGradient(0, 0, float64(qr.Bounds().Dx()), float64(qr.Bounds().Dy()))
	startColor.AddColorStop(0, color.RGBA{R: 0, G: 255, B: 0, A: 255})   // Green
	startColor.AddColorStop(1, color.RGBA{R: 0, G: 0, B: 255, A: 255})   // Blue

	gradient.SetFillStyle(startColor)

	// Draw the gradient rectangle
	gradient.DrawRectangle(0, 0, float64(qr.Bounds().Dx()), float64(qr.Bounds().Dy()))
	gradient.Fill()

	// Apply the gradient color to the QR code
	//draw.Draw(qr, qr.Bounds(), gradient.Image(), image.Point{}, draw.Over)
	gradientImage := gradient.Image()

	for y := 0; y < qr.Bounds().Dy(); y++ {
		for x := 0; x < qr.Bounds().Dx(); x++ {
			qrColor := qr.At(x, y)
			r, g, b, _ := qrColor.RGBA()

			// Check if the QR code pixel is black
			if r == 0 && g == 0 && b == 0 {
				// Apply the gradient color to the black square
				gradientColor := gradientImage.At(x, y)
				qr.Set(x, y, gradientColor)
			}
		}
	}
}

func main() {
	qrCodeGenerator := NewCodeController()
	qrCodeGenerator.GetIcon("Hello World !!", "11111","/workspace/music-store-grpc/static/images/logo.jpg", "/workspace/music-store-grpc/static/images/")
}
